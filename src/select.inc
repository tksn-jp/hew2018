<?php
//////// error report (debug) ///////
// ini_set("display_errors", On);
// error_reporting(E_ALL);
/////////////////////////////////////

function fetch($floorNumber, $colorNumber) {
  $link = mysqli_connect('localhost', 'root', 'root') or die("connection failed");
  mysqli_select_db($link, 'hew2018');
  $query = "
    SELECT
      colorID, AVG(number)
    FROM
      Contributes
    GROUP BY
      floor, colorID
    HAVING
      floor = ${floorNumber} AND colorID = ${colorNumber}
		ORDER BY
			colorID ASC
    ;
  ";

	$query_result = mysqli_query($link, $query);

  $result = mysqli_fetch_assoc($query_result);
  $result["colorID"] = (int)$result["colorID"];
	if (array_key_exists("AVG(number)", $result)) {
	  $result["AVG(number)"] = (float)$result["AVG(number)"];
	} else {
		$result["AVG(number)"] = 0;
	}

	mysqli_close($link);

  if ($result["AVG(number)"] != null) {
    return $result["AVG(number)"];
  } else {
    return 0;
  }
}



?>
