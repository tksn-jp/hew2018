<?php
include '../src/contribute.inc';
include '../src/select.inc';
include '../src/selectedOption.inc';

// "action" が送られていない場合強制的にviewOnlyにする
if (isset($_GET["action"])) {
  $getAction = $_GET["action"];
} else {
  $getAction = 'viewOnly';
}


if ($getAction == 'viewOnly') {
	if (isset($_GET["floor"])) {
	  $getFloor = (int)$_GET["floor"];
	} else {
		$getFloor = 2;
	}
} else if ($getAction == 'contribute') { // 投稿時処理
  $getFloor = (int)$_GET["sendFloor"];
  $getColor = (int)$_GET["color"];
  $getNumber = (int)$_GET["number"];
  Contribute($getFloor, $getColor, $getNumber);
}

// 閲覧処理
// ./src/select.php
$howBlue = fetch($getFloor, 0);
$howRed = fetch($getFloor, 1);
$howGreen = fetch($getFloor, 2);

// エレベータ定員
$maxNumber = 24;

// /////////// debug ///////////////
// var_dump($howBlue);
// print "<br>\n";
// var_dump($howRed);
// print "<br>\n";
// var_dump($howGreen);
// print "<br>\n";
// /////////////////////////////////

?>

<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <title>CocooNavi</title>
  <link rel="stylesheet" href="./css/graph.css">
	<script src="./js/ccchart.js" charset="utf-8"></script>
</head>
<body>
  <div class="wrap">
  	<!-- - - - - - - - - - 上部グローバルメニュー - - - - - - - - - -->
  	<div class="global-menu">
  		<ul>
        <li class="divide"></li>
  			<li><a class="logo" href="./index.html"><img src="./img/logo2.png" alt="トップ画面へ" width="100%" height="100%"></a></li>
        <li class="divide"></li>
  			<li><a href="./howto.html">ABOUT</a></li>
        <li class="divide"></li>
  			<li><a href="./show.php">NOW</a></li>
        <li class="divide"></li>
  		</ul>
  	</div>

		<div class="content">
			<h1><?php print $getFloor;?>階</h1>
			<div class="graphArea">
				<div class="graph blue">
					<canvas id="blueGraph"></canvas>
					<script>
						var blueChart = {
						  "config": {
								"width": 300,
								"height": 300,
						    "title": "青エレベーター",
						    "type": "pie",
						    "useVal": "no",
								"onlyChart": "yes",
								// "onlyChartWidthTitle": "yes",
						    "pieDataIndex": 2,
						    "colNameFont": "100 18px 'Arial'",
						    "pieRingWidth": 80,
						    "pieHoleRadius": 30,
						    "textColor": "#888",
								"colorSet": [
									"rgb(31, 83, 217)",
									"rgba(195, 201, 227, 0.8)"
								],
						    "bg": "#ffffff00"
						  },
						  "data": [
								["", ],
						    ["人数",<?php print $howBlue;?>],
						    ["",<?php print $maxNumber - $howBlue;?>]
						  ]
						};
						ccchart.init('blueGraph', blueChart);
					</script>
					<p><span class="bolder"><?php print $howBlue;?></span> 人待ち</p>
				</div>
				<div class="graph red">
					<canvas id="redGraph"></canvas>
					<script>
						var redChart = {
						  "config": {
								"width": 300,
								"height": 300,
						    "title": "赤エレベーター",
						    "type": "pie",
						    "useVal": "no",
								"onlyChart": "yes",
								// "onlyChartWidthTitle": "yes",
						    "pieDataIndex": 2,
						    "colNameFont": "100 18px 'Arial'",
						    "pieRingWidth": 80,
						    "pieHoleRadius": 30,
						    "textColor": "#888",
								"colorSet": [
									"rgb(226, 36, 10)",
									"rgba(237, 222, 222, 0.8)"
								],
						    "bg": "#ffffff00"
						  },
						  "data": [
								["", ],
						    ["人数",<?php print $howRed;?>],
						    ["",<?php print $maxNumber - $howRed;?>]
						  ]
						};
						ccchart.init('redGraph', redChart);
					</script>
					<p><span class="bolder"><?php print $howRed;?></span> 人待ち</p>
				</div>
				<div class="graph green">
					<canvas id="greenGraph"></canvas>
					<script>
						var greenChart = {
						  "config": {
								"width": 300,
								"height": 300,
						    "title": "緑エレベーター",
						    "type": "pie",
						    "useVal": "no",
								"onlyChart": "yes",
								// "onlyChartWidthTitle": "yes",
						    "pieDataIndex": 2,
						    "colNameFont": "100 18px 'Arial'",
						    "pieRingWidth": 80,
						    "pieHoleRadius": 30,
						    "textColor": "#888",
								"colorSet": [
									"rgb(20, 189, 56)",
									"rgba(204, 230, 208, 0.8)"
								],
						    "bg": "#ffffff00"
						  },
						  "data": [
								["", ],
						    ["人数",<?php print $howGreen;?>],
						    ["",<?php print $maxNumber - $howGreen;?>]
						  ]
						};
						ccchart.init('greenGraph', greenChart);
					</script>
					<p><span class="bolder"><?php print $howGreen;?></span> 人待ち</p>
				</div>
			</div>
      <div class="form">
        <form action="./show.php" method="get">
					<p>
	          <select name="floor" class="selectFloor">
	            <option value="2">2 F</option>
	            <optgroup label="首都医校">
	              <option value="9" <?php selectedOption($getFloor, 9); ?>>9 F</option>
	              <option value="12" <?php selectedOption($getFloor, 12); ?>>12 F</option>
	              <option value="15" <?php selectedOption($getFloor, 15); ?>>15 F</option>
	              <option value="18" <?php selectedOption($getFloor, 18); ?>>18 F</option>
	              <option value="21" <?php selectedOption($getFloor, 21); ?>>21 F</option>
	            </optgroup>
	            <optgroup label="HAL東京">
	              <option value="24" <?php selectedOption($getFloor, 24); ?>>24 F</option>
	              <option value="27" <?php selectedOption($getFloor, 27); ?>>27 F</option>
	              <option value="30" <?php selectedOption($getFloor, 30); ?>>30 F</option>
	              <option value="33" <?php selectedOption($getFloor, 33); ?>>33 F</option>
	              <option value="36" <?php selectedOption($getFloor, 36); ?>>36 F</option>
              </optgroup>
              <optgroup label="東京モード学園">
                <option value="39" <?php selectedOption($getFloor, 39); ?>>39 F</option>
                <option value="42" <?php selectedOption($getFloor, 42); ?>>42 F</option>
                <option value="45" <?php selectedOption($getFloor, 45); ?>>45 F</option>
                <option value="48" <?php selectedOption($getFloor, 48); ?>>48 F</option>
                </optgroup>
              </select>
					</p>
          <button class="moveFloor" type="submit" name="action" value="viewOnly">GO！</button>
					<div class="clear"></div><br />
					<p class="contribute">
						<?php print $getFloor;?>階の
						<select class="color" name="color">
							<option value="0" <?php if (($getFloor > 2 and $getFloor < 24) or $getFloor > 36) {print "disabled";} ?>>青</option>
							<option value="1" <?php if ($getFloor < 30 and $getFloor > 2) {print "disabled";} ?>>赤</option>
							<option value="2" <?php if ($getFloor > 30) {print "disabled";} ?>>緑</option>
						</select>
						エレベーターに
						<input class="inputNumber" type="number" name="number" min="0" max="100" placeholder="人数">人
						<input type="number" name="sendFloor" value="<?php print $getFloor;?>" hidden>
						<button type="submit" class="sendNumber" name="action" value="contribute">待ってるよ！</button>
					</p>
        </form>
			</div>
		</div>
  </div>
</body>
</html>
